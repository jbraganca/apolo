import firebase from '@/firebase';
import store from '@/store';
import db from '@/db';

firebase.auth().onAuthStateChanged((user) => {
  if (user) {
    // User signed in
    if (user.user) {
    // eslint-disable-next-line
      let { user } = user;
    }
    const setUser = {
      id: user.uid,
      name: user.displayName,
      email: user.email,
      image: user.photoURL,
      created_at: firebase.firestore.FieldValue.serverTimestamp(),
    };
    db.collection('users').doc(setUser.id).set(setUser);
    store.commit('auth/setUser', setUser);
    store.commit('auth/getFName', setUser.name);
  } else {
    // User NOT signed in
    store.commit('auth/setUser', null);
  }
});
