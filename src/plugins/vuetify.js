import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#43A047',
    secondary: '#e91e63',
    accent: '#03a9f4',
    error: '#ff5722',
    warning: '#f44336',
    info: '#00bcd4',
    success: '#3f51b5'
  }
});
