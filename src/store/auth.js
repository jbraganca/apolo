import firebase from '@/firebase';

const state = {
  user: {},
  fName: '',
  isLoggedIn: false,
};

const mutations = {
  // eslint-disable-next-line
  setUser(state, user) {
    if (user) {
      // eslint-disable-next-line
      state.user = user;
      // eslint-disable-next-line
      state.isLoggedIn = true;
    } else {
      // eslint-disable-next-line
      state.user = {};
      // eslint-disable-next-line
      state.isLoggedIn = false;
    }
  },
  getFName(state, name) {
    if (name) {
      state.fName = name.split(' ');
      state.fName = state.fName[0];
    } else {
      state.fName = '';
    }
  },
};

const actions = {
  async login() {
    const provider = new firebase.auth.GoogleAuthProvider();
    await firebase.auth().signInWithPopup(provider);
  },
  async logout() {
    await firebase.auth().signOut();
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
