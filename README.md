# Apolo IFMG
#### Projeto de IoT

## O que é?
 O Apolo é uma aplicação Web que trabalha em conjunto com um NodeMCU - responsável pelo feed de dados para o dB-, para que informações referentes à disponibilidade de computadores no laboratório de informática esteja disponível em tempo real para os alunos.

## O que usamos?
 A aplicação Web do Apolo é feito com Vue.js e Firebase, sendo integrados por um sistema embarcado feito em NodeMCU v3.
